This patch directory is for patches that were required for previous versions
of ns-3 (ns-3.30.1) to run the example.  Later versions of ns-3 do not
need to be patched.
