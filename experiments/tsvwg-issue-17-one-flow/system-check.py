#!/usr/bin/env python3
import sys
try:
    import matplotlib
except ImportError:
    print("matplotlib not found")
    sys.exit (1)

try:
    import numpy
except ImportError:
    print("numpy not found")
    sys.exit (1)

sys.exit (0)
